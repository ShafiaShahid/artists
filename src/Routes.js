import React from 'react';
import { Redirect, Route, Switch, BrowserRouter as Router, withRouter } from 'react-router-dom';
import App from './App';

const Routes = () => {
    //this could be extended incase of authentication
    let routes = (
        <Switch>
            <Route path="/" component={App} />
            <Redirect to="/" />
        </Switch>
    )

    return (
        <Router>
            {routes}
        </Router>
    )
}

export default withRouter(Routes)