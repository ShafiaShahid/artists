//get artist actionTypes
export const GET_ARTIST_SUCCESS = 'GET_ARTIST_SUCCESS';
export const GET_ARTIST_FAILURE = 'GET_ARTIST_FAILURE';
export const GET_ARTIST_LOADING = 'GET_ARTIST_LOADING';

//get artist's events actionTypes
export const GET_EVENT_SUCCESS = 'GET_EVENT_SUCCESS';
export const GET_EVENT_FAILURE = 'GET_EVENT_FAILURE';
export const GET_EVENT_LOADING = 'GET_EVENT_LOADING';