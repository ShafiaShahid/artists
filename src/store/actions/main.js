import * as actionTypes from './actionTypes';
import axios from 'axios';
import config from '../../config';

//get artist action function

export const getArtistFailure = (error) => {
    return {
        type: actionTypes.GET_ARTIST_FAILURE,
        error: error
    }
}

export const getArtistSuccess = (data) => {
    return {
        type: actionTypes.GET_ARTIST_SUCCESS,
        data: data
    }
}

export const getArtistLoading = (data) => {
    return {
        type: actionTypes.GET_ARTIST_LOADING,
        data: data
    };
}

export const getArtist = (data, callback) => {
    return (dispatch) => {
        dispatch(getArtistLoading(true));
        return axios.get(`${config.baseApiUrl}/artists/${data}?app_id=123`,
        )
            .then((response) => {
                dispatch(getArtistSuccess(response.data));
                dispatch(getArtistLoading(true));
                callback(response.data);
                dispatch(getArtistLoading(false));
            })
            .catch((err) => {
                dispatch(getArtistLoading(false));
                dispatch(getArtistFailure(err));
                callback(false);
            })
    }
}


// get artist's event action function

export const getEventFailure = (error) => {
    return {
        type: actionTypes.GET_EVENT_FAILURE,
        error: error
    }
}

export const getEventSuccess = (data) => {
    return {
        type: actionTypes.GET_EVENT_SUCCESS,
        data: data
    }
}

export const getEventLoading = (data) => {
    return {
        type: actionTypes.GET_EVENT_LOADING,
        data: data
    };
}

export const getEvent = (data, callback) => {
    return (dispatch) => {
        dispatch(getEventLoading(true));
        return axios.get(`${config.baseApiUrl}/artists/${data}/events?app_id=123`,
        )
            .then((response) => {
                dispatch(getEventSuccess(response.data));
                dispatch(getEventLoading(true));
                callback(response.data);
                dispatch(getEventLoading(false));
            })
            .catch((err) => {
                dispatch(getEventLoading(false));
                dispatch(getEventFailure(err));
                callback(false);
            })
    }
}
