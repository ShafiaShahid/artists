import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    getArtistLoading: false,
    getArtistSuccess: null,
    getArtistFailure: null,

    getEventLoading: false,
    getEventSuccess: null,
    getEventFailure: null,

}

const getArtistFailure = (state, action) => {
    return updateObject(state, { getArtistFailure: action.data, getArtistLoading: false });

}
const getArtistSuccess = (state, action) => {
    return updateObject(state, { getArtistSuccess: action.data, getArtistFailure: false });

}

const getArtistLoading = (state, action) => {
    return updateObject(state, { getArtistLoading: action.data, getArtistFailure: null, getArtistSuccess: false });

}


const getEventFailure = (state, action) => {
    return updateObject(state, { getEventFailure: action.data, getEventLoading: false });

}
const getEventSuccess = (state, action) => {
    return updateObject(state, { getEventSuccess: action.data, getEventFailure: false });

}

const getEventLoading = (state, action) => {
    return updateObject(state, { getEventLoading: action.data});

}

const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.GET_ARTIST_FAILURE:
            return getArtistFailure(state, action)
        case actionTypes.GET_ARTIST_SUCCESS:
            return getArtistSuccess(state, action)
        case actionTypes.GET_ARTIST_LOADING:
            return getArtistLoading(state, action)

        case actionTypes.GET_EVENT_FAILURE:
            return getEventFailure(state, action)
        case actionTypes.GET_EVENT_SUCCESS:
            return getEventSuccess(state, action)
        case actionTypes.GET_EVENT_LOADING:
            return getEventLoading(state, action)

        default:
            return state;
    }

}

export default reducer;

