import reducer from './main';
import * as actionTypes from '../actions/actionTypes'

describe('main reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined,{})).toEqual({
            getArtistLoading: false,
            getArtistSuccess: null,
            getArtistFailure: null,
            getEventLoading: false,
            getEventSuccess: null,
            getEventFailure: null,
        })
    });

})