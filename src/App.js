import { Layout, Menu } from 'antd';
import { withRouter, Route, Switch, Link } from 'react-router-dom'
import Artist from './components/Artist/Artist';
import Events from './components/Events/Events'
import './App.less';

const { Header, Content, Footer } = Layout;

function App() {
  //to set multiple routes
  let route = (
    <Switch>
      <Route exact path='/' render={({ match }) => <Artist match={match} />} />
      <Route exact path='/events' render={({ match }) => <Events match={match} />} />
    </Switch>
  )
  return (
      <Layout className="App-header">
        <Header style={{  width: '100%' }}>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item  key="1"><Link to='/'>Artist</Link></Menu.Item>
          </Menu>
        </Header>
        <Content className="site-layout resp">
          <div 
          className="site-layout-background" 
          style={{ padding: 24, minHeight: 380 }}>
            {route}
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Code Challege ©2020 Created by Shafia</Footer>
      </Layout>
  );
}

export default withRouter(App);
