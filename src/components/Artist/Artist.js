import React, { useState } from 'react';
import { Input, Spin, Result } from 'antd';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as actions from '../../store/actions/index';
import CardResults from '../ReusableComponents/CardResults/CardResults'
import './artist.less';
const { Search } = Input;


const Artist = (props) => {
    //new state variable for search query
    const [query, setQuery] = useState('');
    //new state variable for search results
    const [results, setResults] = useState();
    //new state variable for new search
    const [search, setSearch] = useState(false);


    //function will make an api call and return artist
    const onSearch = value => {
        setResults()
        props.getArtist(value, res => {
            if (!res) {

            } else {
                setResults(res);
                setSearch(true);
            }
        })
    }

    const onChangeValue = e => {
        setQuery(e.target.value);
    }

    //will take user to artists events
    const handleEvent = (data) => {
        sessionStorage.setItem("artistName", data.name);
        props.history.push('/events')

    }

    return (
        <div>
            <Search
                placeholder="Search an artist"
                allowClear
                enterButton="Search"
                size="large"
                onSearch={onSearch}
                onChange={onChangeValue}
                value={query}
            />
            {props.getArtistLoading ? <div className='spin-cl'><Spin size='large' /></div>
                :
                results
                    ? <CardResults data={results} handleEvent={handleEvent} />
                    :
                    <Result
                        status={search ? '500' : "404"}
                        title={search ? "Artist does not exist" : "Search an artist"}
                        subTitle={search ? "Search again" : " And get event details!"}
                    />
            }
        </div>
    );
}

const mapStateToProps = state => {
    return {
        getArtistLoading: state.mainReducer.getArtistLoading,
        getArtistSuccess: state.mainReducer.getArtistSuccess,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getArtist: (data, res) => dispatch(actions.getArtist(data, res)),
        getEvent: (data, res) => dispatch(actions.getEvent(data, res)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Artist))