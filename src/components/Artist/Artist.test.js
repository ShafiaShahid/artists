import React from 'react';
import { configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Artist from './Artist';
import CardResults from '../ReusableComponents/CardResults/CardResults';

configure({adapter: new Adapter()})
describe('<Artist/>', () =>{
    it('should not show until searched', () => {
        const wrapper = shallow(<Artist/>)
        expect(wrapper.find(<CardResults/>)).toHaveLength(0);
    });
})