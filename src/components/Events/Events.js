import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import { withRouter } from 'react-router-dom';
import { Card, PageHeader } from 'antd';
import * as actions from '../../store/actions/index';

import './events.less';

const Events = (props) => {
    //storing events of the artist
    const [results, setResults] = useState();
    let name = sessionStorage.getItem("artistName");

    useEffect(() => {
        //this will fetch data on refresh
        props.getEvent(name, res => {
            if (!res) {

            } else {
                setResults(res)
            }
        })
      }, []);
    
    return (
        <>
            <PageHeader
                className="site-page-header"
                onBack={() => { props.history.push('/') }}
                title={`Events of ${name}`}
            />
            {props.getEventLoading ? 
            <div className='spin-cl'><Spin size='large' /></div>
            :
            <div className='card-div'>

                {results && results.map(data =>
                    <div className='card-dtl' >
                        <Card title={data.description ? data.description : "Event Details"} className='crd-bg'>
                            <p><b>Venue:  </b>{data.venue.name}, {data.venue.country}</p>
                            <p><b>{data.offers[0].type}:  </b> {data.offers[0].status} </p>
                            <p><b>Date:  </b>{data.datetime}</p>
                        </Card>
                    </div>)}
            </div>}
        </>
    );
}

const mapStateToProps = state => {
    return {
        getEventLoading: state.mainReducer.getEventLoading,
        getEventSuccess: state.mainReducer.getEventSuccess,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getArtist: (data, res) => dispatch(actions.getArtist(data, res)),
        getEvent: (data, res) => dispatch(actions.getEvent(data, res)),

    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Events))