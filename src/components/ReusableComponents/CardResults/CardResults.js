import React from 'react';
import {
    Button,
    Form
} from 'antd';
import './cardResults.less';

const CardResults = (props) => {
    return (
        <div className='bg-div'>
            <div className='img-div'>
                <img src={props.data.image_url} className='image-dim'/>
            </div>
            <div className='dtl-div'>
                <Form>
                    <Form.Item label="Artist Name" className="dtl-tag">
                        <span className="dtl-text">{props.data.name} </span>
                    </Form.Item>
                    <Form.Item label="Upcoming Events" className="dtl-tag">
                        <span className="dtl-text">{props.data.upcoming_event_count} </span>
                    </Form.Item>
                    <Form.Item label="Facebook link" className="dtl-tag">
                        <a href={props.data.facebook_page_url} className="dtl-text">{props.data.facebook_page_url} </a>
                    </Form.Item>
                </Form>

                {props.data.upcoming_event_count > 0 && <Button onClick={() => props.handleEvent(props.data)}>Check events</Button>}
            </div>
        </div>
    );
}

export default CardResults